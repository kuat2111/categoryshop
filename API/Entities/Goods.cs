﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    public class Goods
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public string ImageUrl {get;set;}
        public ICollection<GoodsProp> Properties { get; set; }
        
    }
}