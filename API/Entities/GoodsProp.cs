﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    public class GoodsProp
    {
        public int Id { get; set; }
        public int CategoryPropertiesId { get; set; }
        [ForeignKey("CategoryPropertiesId")]
        public CategoryProperties CategoryProperties { get; set; }
        public int GoodsId { get; set; }
        [ForeignKey("GoodsId")]
        public Goods Goods { get; set; }
        public string Value { get; set; }
       
    }
}