﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    public class CategoryProperties
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        public string TitleProparties { get; set; }
    }
}