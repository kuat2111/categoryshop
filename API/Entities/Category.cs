﻿using System.Collections.Generic;

namespace API.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<CategoryProperties> CategoryProperties { get; set; }
    }
}