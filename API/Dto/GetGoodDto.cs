﻿using System.Collections.Generic;
using API.Entities;

namespace API.Dto
{
    public class GetGoodDto
    {
        public string Title { get; set; }
        public string CategoryName { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public string ImageUrl {get;set;}
        
        public ICollection<GoodsProp> GoodsProp { get; set; }
    }
}