﻿using System.Collections.Generic;

namespace API.Dto
{
    public class CategoryPropDto
    {
        public int CategoryPropertiesIds { get; set; }
        public string  GoodProparties { get; set; }
    }
}