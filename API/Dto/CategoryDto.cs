﻿using System.Collections.Generic;
using API.Entities;

namespace API.Dto
{
    public class CategoryDto
    {
      
        public string Title { get; set; }

        public List<string> TitleProparties { get; set; }
    }
}