﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace API.Dto
{
    public class TestDto
    {
        public int Id { get; set; }
        public IFormFile ImageFile { get; set; }
        
        public string ImageUrl { get; set; }
    }
}