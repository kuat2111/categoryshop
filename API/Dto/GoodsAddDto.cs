﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace API.Dto
{
    public class GoodsAddDto
    {
       
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public List<GoodsPropAdd> GoodsPropAdds { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
    }
}