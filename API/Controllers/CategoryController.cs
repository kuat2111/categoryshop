﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data;
using API.Dto;
using API.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController 
    {
        private readonly DataContext _context;
        public CategoryController(DataContext context) 
        {
            _context = context;
        }
       
        [HttpPost("add")]
        public async Task<ActionResult<int>> Add([FromForm]  CategoryDto posts)
        {
            var en = new Category
            {
                Title = posts.Title
            };
            _context.Categories.Add(en);
            await _context.SaveChangesAsync();
            if (posts.TitleProparties != null)
                await AddShopProp(en.Id, posts.TitleProparties);
            return en.Id;

        }
        private async Task AddShopProp(int categoryId, List<string> TitleProparties)
        {
            var titles = new List<CategoryProperties>();
            foreach (var TitlePropartie in TitleProparties)
            {
                titles.Add(new CategoryProperties(){CategoryId = categoryId, TitleProparties = TitlePropartie});
            }
            await _context.CategoryProperties.AddRangeAsync(titles);
            await _context.SaveChangesAsync();
        }
        [HttpDelete("delete/{categoryId}")]
        public async Task Delete(int categoryId)
        {
            var category = _context.Categories.FirstOrDefault(x => x.Id == categoryId); 
            _context.Remove(category);
            _context.SaveChangesAsync();
        }
        
        [HttpGet]
        public async Task<List<Category>> GetAll()
        {
            var result = await _context.Categories
                .Include(w => w.CategoryProperties)
                .Select(x => new Category()
                {
                    Id = x.Id,
                    Title = x.Title,
                    CategoryProperties = x.CategoryProperties
                    
                }).ToListAsync();
            return result;
        }
    }
}