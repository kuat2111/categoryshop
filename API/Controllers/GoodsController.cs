﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Data;
using API.Dto;
using API.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{
    [ApiController]
    [Microsoft.AspNetCore.Components.Route("api/[controller]")]
    public class GoodsController
    {
        private readonly DataContext _context;
        private readonly IConfiguration _config;
        public GoodsController(DataContext context,IConfiguration config) 
        {
            _context = context;
            _config = config;
        }

        [HttpPost("add-photo")]
        public async Task AddPhoto([FromForm] TestDto posts)
        {
            var fileName = await SaveFile(posts.ImageFile);
            posts.ImageUrl = fileName;
            var en = await _context.Goods.Where(x => x.Id == posts.Id).FirstOrDefaultAsync();
            en.ImageUrl = posts.ImageUrl;
            _context.Goods.Update(en);
            await _context.SaveChangesAsync();
        }
        [HttpPost("add")]
        public async Task<int> Add( GoodsAddDto posts)
        {
            var en = new Goods
            {
                CategoryId = posts.CategoryId,
                Title = posts.Title,
                Description = posts.Description,
                Price = posts.Price
            };
            _context.Goods.Add(en);
            await _context.SaveChangesAsync();
            await AddGoodProp(en.Id, posts.GoodsPropAdds);
            return en.Id;
        }
        
        private async Task AddGoodProp(int goodId ,List<GoodsPropAdd> goodsPropAdds)
        {
            var en = new List<GoodsProp>();
            foreach (var CategoryPropertiesIds in goodsPropAdds)
            {
                en.Add(new GoodsProp(){GoodsId = goodId, CategoryPropertiesId = CategoryPropertiesIds.CategoryPropertiesId,Value = CategoryPropertiesIds.GoodProparties });
            }
            await _context.GoodsProps.AddRangeAsync(en);
            await _context.SaveChangesAsync();
        }
        [HttpGet("getCategory")]
        public async Task<List<CategoryPropDto>> GetShopProp(int categoryId)
        {
            var get = await _context.CategoryProperties.Where(x => x.CategoryId == categoryId).Select(
                x=> new CategoryPropDto
                {
                    CategoryPropertiesIds = x.Id, 
                    GoodProparties= x.TitleProparties
                }).ToListAsync();
            return get;
        }
        [HttpGet("getGoodByCategory/{categoryId}")]
        public async Task<List<GetGoodDto>> GetGood(int categoryId)
        {
            var get = await _context.Goods.Include(x=>x.Properties).Where(x => x.CategoryId == categoryId).Select(
                x=> new GetGoodDto
                {
                   Title = x.Title,
                   CategoryName = x.Category.Title,
                   Price = x.Price,
                   Description = x.Description,
                   ImageUrl = _config.GetValue<string>("Goods:FilesPath") + @"\"+ x.ImageUrl,
                   GoodsProp = x.Properties 
                   
                }).ToListAsync();
            return get;
        }
        [HttpGet("getGoodByCategoryProp/{categoryPropId}")]
        public async Task<List<GetGoodDto>> GetGoodByProp(int categoryPropId)
        {
            var get = await _context.Goods.Include(x=>x.Category)
                .Where(x => x.Properties.Select(w=>w.CategoryPropertiesId).Contains(categoryPropId))
                    .Select(
                x=> new GetGoodDto
                {
                    Title = x.Title,
                    CategoryName = x.Category.Title,
                    Price = x.Price,
                    Description = x.Description,
                    ImageUrl = _config.GetValue<string>("Goods:FilesPath") + @"\"+ x.ImageUrl,
                    GoodsProp = x.Properties 
                   
                }).ToListAsync();
            return get;
        }
        [HttpPost("addPhoto")]
        private async Task<string> SaveFile(IFormFile uploadedFile)
        {
            var strFileType = uploadedFile.FileName.Substring(uploadedFile.FileName.LastIndexOf('.')+1);
            var directoryPath = _config.GetValue<string>("Goods:FilesPath");
            var fileName = Guid.NewGuid().ToString() + "." + strFileType;
            var filePath = directoryPath + Path.DirectorySeparatorChar + fileName;
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await uploadedFile.CopyToAsync(fileStream);
            }
            return fileName;
        }
    }
}