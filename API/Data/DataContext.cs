﻿using API.Entities;
using Microsoft.EntityFrameworkCore;

namespace API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Category> Categories { get; set;  }
        public DbSet<CategoryProperties> CategoryProperties { get; set;  }
        public DbSet<Goods> Goods { get; set; }
        public DbSet<GoodsProp> GoodsProps { get; set; }
    }
}